# README #

This is repository for entrance application for Funtasty company. This app download JSON data from data.nasa.gov/resource/ API, stores them in Realm DB and displays in RecyclerView.
Firebase Job Scheduler library was used for periodic data synchronization.

### APK file ###

* APK file is included and can be found in "Funtasty entrance app/app/release/" folder.

### Application icon ###

* Application icon was created by AHED KHALIL. Avaiable at http://globalapk.com/android-apps/8678-meteor-icon-pack-v10.html
