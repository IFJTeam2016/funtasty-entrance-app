package com.funtasty.entrancetest.meteorify;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Google maps activity that is triggerer when item in recyclerview is clicked, show the location of given meteorite
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback{
    GoogleMap map;
    private Double longitude;
    private Double latitude;
    private String name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initDisplay();

        this.longitude = Double.valueOf(getIntent().getStringExtra("longitude"));
        this.latitude = Double.valueOf(getIntent().getStringExtra("latitude"));
        this.name = getIntent().getStringExtra("name");
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    /**
     * Display configuration initialization - opens window as Popup window
     */
    protected void initDisplay() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        getWindow().setLayout((int)(width*0.8), (int)(height*0.6));
    }

    /**
     * Initialize map and display marker of given meteorite
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getApplicationContext());
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.addMarker(new MarkerOptions().position(new LatLng(longitude,latitude)).title(name));

        CameraPosition loc = CameraPosition.builder().target(new LatLng(longitude,latitude)).zoom(3).bearing(0).build();
        map.moveCamera(CameraUpdateFactory.newCameraPosition(loc));
    }
}

