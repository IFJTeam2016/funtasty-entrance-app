package com.funtasty.entrancetest.meteorify.utils;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;

import com.funtasty.entrancetest.meteorify.MainActivity;
import com.funtasty.entrancetest.meteorify.adapters.MeteoriteAdapter;
import com.funtasty.entrancetest.meteorify.interfaces.AsyncCallback;
import com.funtasty.entrancetest.meteorify.models.GeolocationGSON;
import com.funtasty.entrancetest.meteorify.models.MeteoritRealm;
import com.funtasty.entrancetest.meteorify.models.Meteorite;
import com.funtasty.entrancetest.meteorify.models.MeteoriteGSON;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;

/**
 * Downloads and saves JSON data from API to Realm Database
 */
public class FetchData extends AsyncTask<Void, Void, Void> {
    private String URL_string = "https://data.nasa.gov/resource/y77d-th95.json?$where=year >= '2011-01-01T00:00:00'"; // Interested only in meteorites since 2011
    private Realm realm;
    private boolean fromJob;
    private MeteoriteAdapter meteoriteAdapter = null;
    private ArrayList<Meteorite> arraylist = null;
    private ProgressDialog progressDialog = null;
    private AsyncCallback delegate = null;

    public FetchData(boolean fromJob) {
        this.fromJob = fromJob;
        realm = Realm.getDefaultInstance();
    }

    public FetchData(MeteoriteAdapter adapter, ArrayList<Meteorite> list, MainActivity activity) {
        this.progressDialog = new ProgressDialog(activity);
        this.meteoriteAdapter = adapter;
        this.arraylist = list;
        this.delegate = activity;
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onPreExecute() {
        if(!fromJob) {
            super.onPreExecute();
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setTitle("Download");
            progressDialog.setMessage("Data download in process. Please wait.");
            progressDialog.show();
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {

        Uri fetchUri = Uri.parse(URL_string);
        try {
            URL url = new URL(fetchUri.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("user-key", "qkRtgALBZgFhH6LkdRdpexIB3"); // App token identifying user (me)
            urlConnection.connect();

            InputStream inputData = urlConnection.getInputStream();
            if(inputData == null) {
                System.out.println("Nic neprislo");
                return null;  // Nothing to process
            }

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputData));

            Gson gson = new Gson();

            MeteoriteGSON[] results = gson.fromJson(reader,MeteoriteGSON[].class);
            final List<MeteoriteGSON> list = Arrays.asList(results);

            if(!fromJob) {
                arraylist.clear();
            }

            for(int i = 0; i < list.size(); i++) {
                final MeteoriteGSON meteor = list.get(i);

                try {
                    realm = Realm.getDefaultInstance();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            MeteoritRealm meteorite = new MeteoritRealm();

                            meteorite.setId(meteor.getMeteo_id());
                            meteorite.setFall(meteor.getFall());
                            meteorite.setMass(meteor.getMeteo_mass());
                            meteorite.setName(meteor.getMeteo_name());
                            meteorite.setNametype(meteor.getNametype());
                            meteorite.setRecclass(meteor.getRecclass());
                            meteorite.setReclat(meteor.getReclat());
                            meteorite.setReclong(meteor.getReclong());
                            meteorite.setYear(meteor.getYear());

                            GeolocationGSON loc = meteor.getGeolocation();
                            meteorite.setCoordinate_longitude(loc.getLongCoordinates());
                            meteorite.setCoordinate_latitude(loc.getLatCoordinates());

                            realm.copyToRealmOrUpdate(meteorite);

                        if(!fromJob) {
                            Meteorite meteo_ar = new Meteorite(
                                    meteorite.getMass().toString(),
                                    meteorite.getName(),
                                    meteorite.getYear().substring(0,10),
                                    meteorite.getFall(),
                                    meteorite.getCoordinate_longitude(),
                                    meteorite.getCoordinate_latitude());

                            arraylist.add(meteo_ar);
                        }
                        }
                    });
                } finally {
                    if(realm != null) {
                        realm.close();
                    }
                }
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if(!fromJob) {  //If called from sync button or at start, immediately displays loaded meteorites
            delegate.processResult(arraylist.size());
            progressDialog.hide();
            Collections.sort(arraylist, new MeteorComparator());
            meteoriteAdapter.notifyDataSetChanged();
        }
    }
}
