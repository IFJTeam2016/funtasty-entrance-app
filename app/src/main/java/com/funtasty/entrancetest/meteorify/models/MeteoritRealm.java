package com.funtasty.entrancetest.meteorify.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Meteorite object for realm database
 */
public class MeteoritRealm extends RealmObject {
    private String fall;    // Fall / found
    @Required
    @PrimaryKey
    private String id;      // Unique id
    private Double mass;      // Mass in grams
    private String name;    // Name of meteorite
    private String nametype;
    private String recclass;
    private String reclat;  // Recorded latitude
    private String reclong; // Recorded longitude
    private String year;    // Year in ISO8601
    private String coordinate_longitude;
    private String coordinate_latitude;
    private String point;

    public String getCoordinate_longitude() {
        return coordinate_longitude;
    }

    public void setCoordinate_longitude(String coordinate_longitude) {
        this.coordinate_longitude = coordinate_longitude;
    }

    public String getCoordinate_latitude() {
        return coordinate_latitude;
    }

    public void setCoordinate_latitude(String coordinate_latitude) {
        this.coordinate_latitude = coordinate_latitude;
    }

    public String getFall() {
        return fall;
    }

    public void setFall(String fall) {
        this.fall = fall;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getMass() {
        return mass;
    }

    public void setMass(Double mass) {
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNametype() {
        return nametype;
    }

    public void setNametype(String nametype) {
        this.nametype = nametype;
    }

    public String getRecclass() {
        return recclass;
    }

    public void setRecclass(String recclass) {
        this.recclass = recclass;
    }

    public String getReclat() {
        return reclat;
    }

    public void setReclat(String reclat) {
        this.reclat = reclat;
    }

    public String getReclong() {
        return reclong;
    }

    public void setReclong(String reclong) {
        this.reclong = reclong;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
