package com.funtasty.entrancetest.meteorify.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MeteoriteGSON {
    public String fall;

    public GeolocationGSON geolocation;

    @SerializedName("id")
    public String meteo_id;

    @SerializedName("mass")
    public Double meteo_mass;

    @SerializedName("name")
    public String meteo_name;

    public String nametype;

    public String recclass;

    public String reclat;

    public String reclong;

    public String year;

    public String getFall() {
        return fall;
    }

    public GeolocationGSON getGeolocation() {
        return geolocation;
    }

    public String getMeteo_id() {
        return meteo_id;
    }

    public Double getMeteo_mass() {
        return meteo_mass;
    }

    public String getMeteo_name() {
        return meteo_name;
    }

    public String getNametype() {
        return nametype;
    }

    public String getRecclass() {
        return recclass;
    }

    public String getReclat() {
        return reclat;
    }

    public String getReclong() {
        return reclong;
    }

    public String getYear() {
        return year;
    }
}

