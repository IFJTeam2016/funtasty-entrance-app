package com.funtasty.entrancetest.meteorify.jobs;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.funtasty.entrancetest.meteorify.utils.FetchData;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Firebase job scheduler job that is planned to synchronize data every day
 */

public class UpdateJob extends JobService{
    private BackgroundTask mBackgroundTask;

    @SuppressLint("StaticFieldLeak")
    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        mBackgroundTask = new BackgroundTask(){

            /**
             * Initializing database where the results will be saved
             */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Realm.init(getApplicationContext());
                RealmConfiguration config = new RealmConfiguration
                        .Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build();
                Realm.setDefaultConfiguration(config);
            }

            protected void onPostExecute(String a) {

                jobFinished(jobParameters,false);
            }
        };

        mBackgroundTask.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }

    public class BackgroundTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            System.out.println("Fetching data for the second time");
            new FetchData(true).execute();      // Fetching data, updating database
            return null;
        }
    }
}
