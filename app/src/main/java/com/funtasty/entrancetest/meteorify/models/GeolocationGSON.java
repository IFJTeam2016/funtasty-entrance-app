package com.funtasty.entrancetest.meteorify.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeolocationGSON {
    @SerializedName("type")
    private String loc_type;
    private List<String> coordinates;

    public String getLoc_type() {
        return loc_type;
    }

    public String getLongCoordinates() {
        return coordinates.get(1);
    }

    public String getLatCoordinates() {
        return coordinates.get(0);
    }
}
