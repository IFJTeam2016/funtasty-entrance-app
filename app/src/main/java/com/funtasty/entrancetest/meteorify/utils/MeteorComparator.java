package com.funtasty.entrancetest.meteorify.utils;

import com.funtasty.entrancetest.meteorify.models.Meteorite;

import java.util.Comparator;

/**
 * Compare meteorites to sort them later in arraylist
 */
public class MeteorComparator implements Comparator<Meteorite> {
    @Override
    public int compare(Meteorite meteorite, Meteorite t1) {
        return (int) (Double.parseDouble(t1.getMass()) - Double.parseDouble(meteorite.getMass()));
    }
}
