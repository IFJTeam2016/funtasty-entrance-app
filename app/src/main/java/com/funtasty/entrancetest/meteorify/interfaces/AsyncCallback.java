package com.funtasty.entrancetest.meteorify.interfaces;

/**
 * Callback interface to pass data from Asynctask FechData back to MainActivity
 */
public interface AsyncCallback {
    void processResult(int count);
}
