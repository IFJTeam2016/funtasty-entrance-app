package com.funtasty.entrancetest.meteorify.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.funtasty.entrancetest.meteorify.MapActivity;
import com.funtasty.entrancetest.meteorify.R;
import com.funtasty.entrancetest.meteorify.models.Meteorite;

import java.util.ArrayList;

/**
 * Adapter for Recyclerview
 */

public class MeteoriteAdapter extends RecyclerView.Adapter<MeteoriteAdapter.MeteoriteHolder>{
    private ArrayList<Meteorite> mData;
    private Context context;

    public MeteoriteAdapter(ArrayList<Meteorite> mData, Context context) {
        this.mData = mData;
        this.context = context;
    }

    @Override
    public MeteoriteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meteorite_row, parent, false);
        return new MeteoriteHolder(view, this.mData, this.context);
    }

    @Override
    public void onBindViewHolder(MeteoriteHolder holder, int position) {
        Meteorite meteorite = mData.get(position);
        holder.setMass(meteorite.getMass());
        holder.setName(meteorite.getName());
        holder.setState(meteorite.getState());
        holder.setDate(meteorite.getYear());
        holder.setLong(meteorite.getLongitude());
        holder.setLat(meteorite.getLatitude());
    }

    @Override
    public int getItemCount() {
        if(mData == null) return 0;
        else {
            return mData.size();
        }
    }

    public class MeteoriteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView meteorite_mass_tv;
        TextView meteorite_name_tv;
        TextView meteorite_date_tv;
        TextView meteorite_state_tv;
        TextView meteorite_long_tv;
        TextView meteorite_lat_tv;
        ArrayList<Meteorite> meteorites = new ArrayList<>();
        Context context;

        public MeteoriteHolder(View itemView, ArrayList<Meteorite> meteorites, Context ctx) {
            super(itemView);

            this.meteorites = meteorites;
            this.context = ctx;

            itemView.setOnClickListener(this);
            meteorite_mass_tv = itemView.findViewById(R.id.meteorite_mass);
            meteorite_name_tv = itemView.findViewById(R.id.meteorite_name);
            meteorite_date_tv = itemView.findViewById(R.id.meteorite_date);
            meteorite_state_tv = itemView.findViewById(R.id.meteorite_state);
            meteorite_long_tv = itemView.findViewById(R.id.meteorite_long);
            meteorite_lat_tv = itemView.findViewById(R.id.meteorite_lat);

        }

        public void setMass(String mass) {
            meteorite_mass_tv.setText(mass);
        }

        public void setName(String name) {
            meteorite_name_tv.setText(name);
        }

        public void setDate(String date) {
            meteorite_date_tv.setText(date);
        }

        public void setState(String state) {
            meteorite_state_tv.setText(state);
        }

        public void setLong(String longitude) {
            meteorite_long_tv.setText(longitude);
        }

        public void setLat(String latitude) {
            meteorite_lat_tv.setText(latitude);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Meteorite selectedMeteorite = this.meteorites.get(position);
            System.out.println(selectedMeteorite.getName());

            Intent intent = new Intent(context,MapActivity.class);
            intent.putExtra("longitude",selectedMeteorite.getLongitude());
            intent.putExtra("latitude",selectedMeteorite.getLatitude());
            intent.putExtra("name",selectedMeteorite.getName());

            this.context.startActivity(intent);
        }
    }
}
