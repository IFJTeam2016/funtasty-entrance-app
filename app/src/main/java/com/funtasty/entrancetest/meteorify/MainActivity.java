package com.funtasty.entrancetest.meteorify;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.funtasty.entrancetest.meteorify.adapters.MeteoriteAdapter;
import com.funtasty.entrancetest.meteorify.interfaces.AsyncCallback;
import com.funtasty.entrancetest.meteorify.jobs.UpdateJob;
import com.funtasty.entrancetest.meteorify.models.MeteoritRealm;
import com.funtasty.entrancetest.meteorify.models.Meteorite;
import com.funtasty.entrancetest.meteorify.utils.FetchData;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity implements AsyncCallback {
    RecyclerView recyclerView;
    MeteoriteAdapter meteoriteAdapter;
    ArrayList<Meteorite> arrayList;
    Realm realm;
    ImageButton sync_button;
    TextView state_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createMenu();
        initialize();
        
        realm = Realm.getDefaultInstance();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if(!preferences.getBoolean("not_virgin",false)) {       // First boot
            realm.close();
            new TestConnectivity().execute();       // Test internet connectivity of first boot, in online, downloads JSON data into Realm

            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("not_virgin", true);
            editor.apply();
        }
        else if(!realm.isEmpty()) {     // If it isnt first boot, it check is Realm DB contains any data, if so display them
            addAllMeteorites();
            realm.close();
        }
        else if(realm.isEmpty()) {      // Database is empty and there is no connection - nothing to do
            preferences.edit().remove("not_virgin").apply();
            realm.close();
        }
    }

    /**
     *
     * @param dispatcher Initializded Firebase Job Dispatcher that will run scheduled job
     * @return Initialized job
     */
    private Job setUp(FirebaseJobDispatcher dispatcher) {
        final int periodicity = (int) TimeUnit.DAYS.toSeconds(24);
        final int tolerance = (int) TimeUnit.MINUTES.toSeconds(30);

        Job job = dispatcher.newJobBuilder()
            .setService(UpdateJob.class)
            .setLifetime(Lifetime.FOREVER)
            .setTag("update_db_job")
            .setRecurring(true)       //Repeatable
            .setConstraints(Constraint.ON_ANY_NETWORK)      // Runs only when network is avaiable
            .setTrigger(Trigger.executionWindow(periodicity,periodicity + tolerance))
            .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
            .setReplaceCurrent(false)
            .build();
        return job;
    }

    /**
     * FetchData callback with the amount of meteorites
     * @param count Number of meteorites in database
     */
    @Override
    public void processResult(int count) {
        String textToSet = "Meteorites: " + arrayList.size();
        state_view.setText(textToSet);
    }

    /**
     * Test network connectivity
     */
    @SuppressLint("StaticFieldLeak")
    public class TestConnectivity extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            Socket socket = new Socket();
            SocketAddress address = new InetSocketAddress("8.8.8.8", 53);

            try {
                socket.connect(address, 1500);
                socket.close();
            } catch (IOException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean online) {
            if(online) {
                new FetchData(meteoriteAdapter,arrayList,MainActivity.this).execute();
            }
            else {
                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Connection error");
                alertDialog.setMessage("Couldn't connect to internet, try again later");
                alertDialog.setCancelable(false);
                alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        }
    }

    /**
     * Initialize the recyclerview and needed arraylist + adapter
     */
    public void initialize() {
        arrayList = new ArrayList<>();
        meteoriteAdapter = new MeteoriteAdapter(arrayList, MainActivity.this);
        recyclerView = findViewById(R.id.meteorite_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(meteoriteAdapter);

        initRealm();

        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher (      // Firebase Job Dispatcher initialization
                new GooglePlayDriver(MainActivity.this)
        );

        Job job = setUp(dispatcher);
        dispatcher.mustSchedule(job);   // Database updates are planned 24 hours in advance
    }

    /**
     * Realm Database initialization
     */
    public void initRealm(){
        Realm.init(getApplicationContext());    // Realm initialization
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    /**
     * Adds all meteorites to array list, which is displayed by recyclerview
     */
    public void addAllMeteorites() {
        realm = Realm.getDefaultInstance();
        RealmResults<MeteoritRealm> result = realm.where(MeteoritRealm.class).findAllSorted("mass", Sort.DESCENDING);
        for(MeteoritRealm meteoritRealm : result) {
            Meteorite meteorite = new Meteorite(
                    meteoritRealm.getMass().toString(),
                    meteoritRealm.getName(),
                    meteoritRealm.getYear().substring(0,10),
                    meteoritRealm.getFall(),
                    meteoritRealm.getCoordinate_longitude(),
                    meteoritRealm.getCoordinate_latitude());
            arrayList.add(meteorite);
        }
        String textToSet = "Meteorites: " + arrayList.size();
        state_view.setText(textToSet);
        realm.close();
    }

    /**
     * Creates custom menu
     */
    private void createMenu() {
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);
        View view = getSupportActionBar().getCustomView();

        sync_button = view.findViewById(R.id.sync_btn);
        sync_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TestConnectivity().execute();
            }
        });

        state_view = view.findViewById(R.id.state_tv);
    }


}
