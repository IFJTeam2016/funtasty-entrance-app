package com.funtasty.entrancetest.meteorify.models;

public class Meteorite {
    private String mass;      // Mass in grams
    private String name;    // Name of meteorite
    private String year;    // Year in ISO8601
    private String state;
    private String longitude;
    private String latitude;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Meteorite(String mass, String name, String year, String state, String longitude, String latitude) {
        this.mass = mass;
        this.name = name;
        this.year = year;
        this.state = state;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
